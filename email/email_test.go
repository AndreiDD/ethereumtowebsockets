package email

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// if you haven't setup the sendgrid api, you'll get 401 error code
func TestSendEmail(t *testing.T) {
	assert := assert.New(t)
	statusCode, err := SendEmail()
	assert.Nilf(err, "error message %s", "formatted")
	assert.Equal(statusCode, 200, "status code should be 200")
}
