package email

import (
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// SendEmail simple send email function.
// Return the statusCode on success or error on failure
func SendEmail() (int, error) {

	// the payload
	from := mail.NewEmail("Example User", "test@example.com")
	subject := "Sending with SendGrid is Fun"
	to := mail.NewEmail("Example User", "test@example.com")
	plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := "<strong>and easy to do anywhere, even with Go</strong>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)

	// sending it to sendgrid
	client := sendgrid.NewSendClient("PUT HERE YOUR SENDGRID API")
	response, err := client.Send(message)
	if err != nil {
		return -1, err
	}

	return response.StatusCode, nil
}
