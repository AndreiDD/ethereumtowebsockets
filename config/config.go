package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Load the config files
func Load() *viper.Viper {
	config, err := readConfig("./baseconfig", map[string]interface{}{
		"contract_address":       "0x0000000000",
		"node_url":     "xxxx",
	})
	if err != nil {
		log.Errorf("error when reading config: %v\n", err)
	}
	return config
}

//read the config file, helper function
func readConfig(filename string, defaults map[string]interface{}) (*viper.Viper, error) {
	v := viper.New()
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
	v.SetConfigName(filename)
	v.AddConfigPath(".")
	v.AddConfigPath("./config")
	v.AutomaticEnv()
	err := v.ReadInConfig()
	return v, err
}
