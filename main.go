package main

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	"math/big"
	"net/http"
	"strings"
	"txmonitoring/config"
	"txmonitoring/eth"
)

// setting up some channels for ws
var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan string)
var upgrade = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	configuration := config.Load()
	log.Println("Tx Monitoring Service")
	log.Println("version: 0.1d listening for " + configuration.GetString("contract_address") + " @ " + configuration.GetString("node_url"))

	// Init the ETH Client
	if err := eth.InitEthClient(configuration.GetString("node_url")); err != nil {
		log.Fatalf("cannot connect to the ETH provider. Please check if you have Geth running: %s", err)
	}

	// Listen up for events
	go func() {

		logTransferSig := []byte("Transfer(address,address,uint256)")
		logTransferSigHash := crypto.Keccak256Hash(logTransferSig)

		contractAddress := common.HexToAddress(configuration.GetString("contract_address"))
		query := ethereum.FilterQuery{
			Addresses: []common.Address{contractAddress},
		}

		contractAbi, err := abi.JSON(strings.NewReader(eth.TokenABI))
		if err != nil {
			log.Fatal(err)
		}

		logs := make(chan types.Log)
		sub, err := eth.ETHClient.SubscribeFilterLogs(context.Background(), query, logs)
		if err != nil {
			log.Error(err)
		}
		for {
			select {
			case err := <-sub.Err():
				log.Error(err)
			case vLog := <-logs:

				if vLog.Topics[0].Hex() == logTransferSigHash.Hex() {

					var event = struct {
						Value *big.Int // must named "Value", dont' change.
					}{}
					err = contractAbi.Unpack(&event, "Transfer", vLog.Data)
					if err != nil {
						log.Errorf("Subscription unpacking error: %s", err)
						continue
					}

					amount := decimal.NewFromBigInt(event.Value, 0)
					fromAddress := common.BytesToAddress(vLog.Topics[1].Bytes()).String()
					toAddress := common.BytesToAddress(vLog.Topics[2].Bytes()).String()
					txHash := vLog.TxHash.String()

					payload := "Amount: " + amount.String() + " wei | From: " + fromAddress + " | To: " + toAddress + " | Tx Hash:" + txHash
					fmt.Println(payload)

					//update the webpage
					broadcast <- payload

					//sending email here -> email.SendEmail(payload)

				}
			}
		}
	}()

	// handling ws
	router := mux.NewRouter()
	router.HandleFunc("/ws", wsHandler)
	go sendToWS()

	log.Fatal(http.ListenAndServe(":5556", router))
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrade.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}

	// register client
	clients[ws] = true
}

// listens for an incoming broadcast
func sendToWS() {
	for {
		val := <-broadcast

		for client := range clients {
			err := client.WriteMessage(websocket.TextMessage, []byte(val))
			if err != nil {
				log.Printf("Websocket error: %s", err)
				_ = client.Close()
				delete(clients, client)
			}
		}
	}
}
