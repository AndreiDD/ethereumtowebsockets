# Tx Monitoring

### Video how it works here: 

<a href="https://drive.google.com/file/d/1JZ7VdcyneAb3FA6VCb3CR1i9YCBDNW-W/view?usp=sharing">presentation video for this repository</a>

This small service listens for events @ a specific contract

- Edit the /config/baseconfig.json file

~~~~
{
  "contract_address" : "0x81c336dd82702db36fa3a70892d753f100827a48",
  "node_url" : "ws://127.0.0.1:8545"
}
~~~~

- environment: debug or release
- contract_address: your contract that you want to monitor
- node_url: the rpc URL for connecting to your node (attention, don't use infura since they
don't support listening to events)
- hostname: the hostname this server should run to
- port: the port this server should run to

## Build

- make sure you have the latest go version
- go run *.go in the project directory

## Troubleshooting

- x
- y 
- z


