package eth

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/ethereum/go-ethereum/ethclient"
)

// ETHClient is the ETH client
var ETHClient *ethclient.Client

// InitEthClient fires up the client
func InitEthClient(url string) error {

	ethClient, err := ethclient.Dial(url)

	if err != nil {
		log.Errorf("Failed to connect to the ETH client: %v", err)
		return err
	}

	ETHClient = ethClient
	fmt.Printf("Connected to the ETH provider: %s\n", url)
	return nil

}
